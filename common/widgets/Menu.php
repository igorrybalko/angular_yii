<?php
namespace common\widgets;

use frontend\models\Menu as modelMenu;

class Menu{

    private static function getMenu($id){
        return modelMenu::getItems($id);
    }

    public static function render($id){
        $menuObject = self::getMenu($id);
        $tpl = '<ul class="nav">';

        foreach ($menuObject as $menuItem){
            $tpl .= '<li class="menu-item-id-' . $menuItem["id"] . '">'
            . '<a href="' . $menuItem["url"] . '">' . $menuItem["title"] . '</a></li>';
        }
        $tpl .= '</ul>';
        return $tpl;
    }

}