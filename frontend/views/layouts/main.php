<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\Menu;

AppAsset::register($this);

require_once (__DIR__ . '/../ink/layout-header.php');?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <base href="/" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->params['title']) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= $bodyClass; ?>">
<?php $this->beginBody() ?>
<app-root>
    <div class="cssload-wrap">
        <div class="cssload-circle"></div>
        <div class="cssload-circle"></div>
        <div class="cssload-circle"></div>
        <div class="cssload-circle"></div>
        <div class="cssload-circle"></div>
    </div>
</app-root>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
