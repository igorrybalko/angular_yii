<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.min.css',
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBIwbwEqOKfd9kpEmfSxKgcL0CFPDtLoeE',
        'js/polyfills.js',
        'js/app.js',

    ];
    public $depends = [
        //'yii\web\YiiAsset',
    ];
}
