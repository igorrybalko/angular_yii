import { Component, OnInit} from '@angular/core';
import { HttpGetService} from '../services/httpGet.service';
import { ActivatedRoute} from '@angular/router';

interface BlogItemData {
    id: number,
    title: string,
    content: string,
    cat_id: number,
    img: string,
    createdate: string
}

@Component({
    selector: 'blogitem-app',
    templateUrl: '../../tpl/pages/blogItemPage.html',
    providers: [HttpGetService]
})
export class BlogItemComponent implements OnInit{

    articleData: BlogItemData = {
        id: 0,
        title: '',
        content: '',
        cat_id: 0,
        img: '',
        createdate: ''
    };
    slug: string;

    constructor(
        private httpService: HttpGetService,
        private activateRoute: ActivatedRoute
    ){
        this.slug = activateRoute.snapshot.params['slug'];
    }

    ngOnInit(){
        this.httpService.getData('/news/get-data/' + this.slug).subscribe((response: BlogItemData) => this.articleData = response);
    }
}