import { Component, OnInit, OnChanges, NgZone} from '@angular/core';
import { HttpGetService} from '../services/httpGet.service';
import { ActivatedRoute, Params} from '@angular/router';

interface BlogItemData {
    title: string,
    slug: string,
    id: number,
    content: string,
    cat_id: number,
    img: string,
    createdate: string,
    cat_slug: string
}

interface BlogData {
    items: Array<BlogItemData>,
    cat_slug: string,
    lastPageNum: number,
    currPageNum: number,
    paginNums: number[]
}

@Component({
    selector: 'news-app',
    templateUrl: '../../tpl/pages/blogPage.html',
    providers: [HttpGetService]
})
export class NewsComponent implements OnInit, OnChanges{

    blogData: BlogData = {
        items: [
            {
                title: '',
                slug: '',
                id: 0,
                content: '',
                cat_id: 0,
                img: '',
                createdate: '',
                cat_slug: ''
            }
        ],
        cat_slug: '',
        lastPageNum: 0,
        currPageNum: 0,
        paginNums: [0]
    };

    imutUrl:string = '/category-items/news';

    url:string = this.imutUrl;

    constructor(private httpService: HttpGetService,
                private activatedRoute: ActivatedRoute,
                private zone: NgZone){
    }

    ngOnChanges(){
        console.log('onChanges news.component');
    }

    ngOnInit(){

        this.activatedRoute.queryParams.subscribe((params: Params) =>{
            if(params['page']){
                this.url = this.imutUrl + '?page=' + params['page'];
            }else{
                this.url = this.imutUrl
            }
            this.httpService.getData(this.url).subscribe((response: BlogData) => {
                this.zone.run(() => this.blogData = response);
            });

        });
    }
}