import { Component, AfterViewInit } from '@angular/core';
import AppHelper from '../helpers/appHelper';
declare var $:any;

@Component({
    selector: 'app-root',
    templateUrl: '../../tpl/appRoot.html'
})
export class AppComponent implements AfterViewInit{

   ngAfterViewInit(){

       let appHelper:AppHelper = new AppHelper();

       appHelper.modal();

       $(window).on('scroll', function() {
           if($(this).scrollTop() > 100) {
               $('#toTop').fadeIn();
           } else {
               $('#toTop').fadeOut();
           }
       });
   }
}