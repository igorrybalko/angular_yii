import { Input, Component } from '@angular/core';

@Component({
    selector: 'pagination-chunk',
    templateUrl: '../../tpl/chunks/paginationChunk.html'
})
export class PaginationComponent {
    @Input() lastPageNum: number;
    @Input() currPageNum: number;
    @Input() paginNums: number[];
    @Input() catSlug: string;
}