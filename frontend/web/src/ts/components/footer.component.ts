import { Component } from '@angular/core';
declare var $:any;

@Component({
    selector: 'footer-chunk',
    templateUrl: '../../tpl/chunks/footerChunk.html'
})
export class FooterComponent {

    toTop(){

        $('body,html').animate({scrollTop:0},800);

    }
}