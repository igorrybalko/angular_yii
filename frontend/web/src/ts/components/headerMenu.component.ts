import { Component , OnInit} from '@angular/core';
import { HttpGetService} from '../services/httpGet.service';

@Component({
    selector: 'header-menu-chunk',
    templateUrl: '../../tpl/chunks/headerMenuChunk.html',
    providers: [HttpGetService]
})
export class HeaderMenuComponent implements OnInit{

    menuItems: object[] = [{}];

    constructor(private httpService: HttpGetService){}

    ngOnInit(){
        this.httpService.getData('/menu/items?id=1').subscribe((data:object[]) => this.menuItems = data);
    }

}