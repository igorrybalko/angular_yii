import { Component, OnInit} from '@angular/core';
import { HttpGetService} from '../services/httpGet.service';

interface SimplePageData{
    content: string,
    title: string,
    img: string,
    createdate: string,
    id: number,
    hasimg: boolean
}

@Component({
    selector: 'about-app',
    templateUrl: '../../tpl/pages/simplePage.html',
    providers: [HttpGetService]
})
export class AboutComponent implements OnInit{

    data: SimplePageData = {
        content: '',
        title: '',
        img: '',
        createdate: '',
        id: 0,
        hasimg: false
    };

    constructor(private httpService: HttpGetService){}

    ngOnInit(){
        this.httpService.getData('/simple-pages/2').subscribe((response: SimplePageData) => this.data = response);
    }
}