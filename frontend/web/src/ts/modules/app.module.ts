import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent }   from '../components/app.component';
import { AboutComponent }   from '../components/about.component';
import { HomeComponent }   from '../components/home.component';
import { NewsComponent }   from '../components/news.component';
import { BlogItemComponent }   from '../components/blogItem.component';
import { NotFoundComponent }   from '../components/not-found.component';

import { HeaderComponent }   from '../components/header.component';
import { FooterComponent }   from '../components/footer.component';
import { PaginationComponent }   from '../components/pagination.component';
import { HeaderMenuComponent }   from '../components/headerMenu.component';

// определение маршрутов
const appRoutes: Routes =[
    { path: '', component: HomeComponent},
    { path: 'about', component: AboutComponent},
    { path: 'news', component: NewsComponent},
    { path: 'news/:slug', component: BlogItemComponent},
    { path: '**', component: NotFoundComponent }
];


@NgModule({
    imports:      [ BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes) ],
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        NewsComponent,
        BlogItemComponent,
        NotFoundComponent,
        HeaderComponent,
        FooterComponent,
        HeaderMenuComponent,
        PaginationComponent
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
