<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jaHxgV4IybhHWz-G_K-rAh9Eam9Uepbi',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
//                'yii\web\JqueryAsset' => [
//                    'js' => [
//                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
//                    ]
//                ],
                'yii\web\JqueryAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
//                'yii\bootstrap\BootstrapAsset' => [
//                    'css' => [
//                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
//                    ]
//                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ]
//                'yii\bootstrap\BootstrapPluginAsset' => [
//                    'js' => [
//                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
//                    ]
//                ]
            ],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
