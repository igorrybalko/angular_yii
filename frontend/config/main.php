<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
//        ],
        'user' => [
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
//        'session' => [
//            // this is the name of the session cookie used for login on the frontend
//            'name' => 'advanced-frontend',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing' => false,
            'rules' => [
                '' => 'site/index',
                'menu/items' => 'menu/items',
                //'<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
//                'categorylist' => 'categories/index',
                    'news' => 'categories/items-category',
                    'category-items/<slug>' => 'categories/get-items-category',
                    'about' => 'simple-pages/about',
                    'simple-pages/<id>' => 'simple-pages/get-data',
//                'some' => 'categories/category',
                    'news/<slug>' => 'article/index',
                    'news/get-data/<slug>' => 'article/get-article',

//                'news/view/<slug>' => 'news/view',
    //            'GET news/<slug>'  => 'news/view',
   //             'catalog/products' => 'catalog/products',
//                'catalog/get-com-pred/<id>' => 'catalog/get-com-pred',
//                'catalog/view/<slug>'  => 'catalog/view',
//                'GET catalog/<slug>'  => 'catalog/view',
                // 'page/view/<slug>' => 'page/view',
                // 'GET page/<slug>'  => 'page/view',
//                'services/view/<slug>' => 'services/view',
                //'sitemap.xml'      => 'site/sitemap',
//                'GET <slug>'  => 'page/view',
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'user' => [
            // following line will restrict access to admin controller from frontend application
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
        ],
    ],
];
