<?php

namespace frontend\controllers;

use Yii;
use yii\db\Query;
use yii\data\Pagination;

class CategoriesController extends \yii\web\Controller
{

    public function actionItemsCategory()
    {
        Yii::$app->view->params['title'] = 'News';
        return $this->render('../site/index');
    }

    public function actionGetItemsCategory($slug)
    {

        $i = 0;
        $paginNums = [];

        $pageNumber = (int) Yii::$app->request->get('page');
        if(!$pageNumber){
            $pageNumber = 0;
        }

        $query = new Query();
        $select = ['a.title', 'a.slug', 'a.id', 'a.content', 'a.cat_id', 'a.img', 'a.createdate', 'c.slug as cat_slug'];
        $result = $query->
            select($select)->
            from('articles a')->
            innerJoin('categories c', 'c.id = a.cat_id')->
            where([
                'a.published' => 1,
                'c.slug' => $slug
            ]);

        // делаем копию выборки
        $countQuery = clone $result;
        $pageSize = 2;
        $countPages = $countQuery->count();
        $lastPageNum = ceil($countPages/$pageSize) - 1;
        if($pageNumber > $lastPageNum || $pageNumber < 0){
            $pageNumber = 0;
        }

        $pages = new Pagination(['totalCount' => $countPages, 'pageSize' => $pageSize]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $result->offset($pageNumber * $pageSize)
            ->limit($pages->limit)
            ->all();

        foreach ( $models as $el){
            $models[$i]['content'] = substr($models[$i]['content'], 0, 200) . '...';
            $i++;
        }

        for($j = 0; $j < $lastPageNum; $j++){
            $paginNums[] = $j;
        }

        $data = [
            'items' => $models,
            'cat_slug' => $slug,
            'lastPageNum' => $lastPageNum,
            'currPageNum' => $pageNumber,
            'paginNums' => $paginNums
        ];

        return json_encode($data);
    }
}