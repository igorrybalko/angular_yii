<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Menu;

class MenuController extends \yii\web\Controller
{
    public function actionItems()
    {
        $id = Yii::$app->request->get('id');
        $menuItems = json_encode(Menu::getItems($id));
        return $menuItems;

    }

}