<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\SimplePages;

class SimplePagesController extends Controller
{

    public function actionAbout()
    {
        Yii::$app->view->params['title'] = 'About';
        return $this->render('../site/index');
    }

    public function actionGetData()
    {
        $id = Yii::$app->request->get('id');
        $res = SimplePages::find()->where(['id' => $id, 'published' => 1])->one();

        $data = [
            'content' => $res->content,
            'title' => $res->title,
            'img' => $res->img,
            'createdate' => $res->createdate,
            'id' => $res->id,
            'hasimg' => ($res->img ? true : false)
        ];

        return json_encode($data);
    }
}
