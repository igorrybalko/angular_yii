<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Article;

class ArticleController extends \yii\web\Controller
{
    public function actionIndex($slug)
    {
        $article = Article::find()->where(['slug' => $slug, 'published' => 1])->one();
        Yii::$app->view->params['title'] = $article->title;
        return $this->render('../site/index');
    }

    public function actionGetArticle($slug)
    {
        $article = Article::find()->where(['slug' => $slug, 'published' => 1])->one();

        $data = [
            'id' => $article->id,
            'title' => $article->title,
            'content' => $article->content,
            'cat_id' => $article->cat_id,
            'img' => $article->img,
            'createdate' => $article->createdate
        ];

        return json_encode($data);
        
    }

}
