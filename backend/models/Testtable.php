<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "testtable".
 *
 * @property integer $id
 * @property integer $published
 * @property string $title
 * @property string $content
 * @property string $pubdate
 * @property string $updated_at
 */
class Testtable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testtable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['published'], 'integer'],
            [['title', 'content', 'pubdate'], 'required'],
            [['content'], 'string'],
            [['pubdate', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'published' => Yii::t('app', 'Published'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'pubdate' => Yii::t('app', 'Pubdate'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
