<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menuitems".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $menuid
 * @property integer $published
 * @property string $createdate
 */
class MenuItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menuitems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'menuid'], 'required'],
            [['menuid', 'published', 'order'], 'integer'],
            [['createdate'], 'safe'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'menuid' => Yii::t('app', 'Menu'),
            'order' => Yii::t('app', 'Order'),
            'published' => Yii::t('app', 'Published'),
            'createdate' => Yii::t('app', 'Createdate'),
        ];
    }
}
