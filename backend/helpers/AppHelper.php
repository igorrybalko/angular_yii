<?php

namespace backend\helpers;

use Yii;

class AppHelper{

    static function onBeforeAllActions(){
        if (Yii::$app->user->isGuest) {
            $currAction = Yii::$app->controller->action->id;
            $currCtrl = Yii::$app->controller->id;
            if($currAction != 'login' && $currCtrl != 'security') {
                Yii::$app->response->redirect('/');
            }
        }
    }
}