($=>{

    $(()=>{

        $('.module-form #w0').on('submit', function (e) {
            
            let customFieldData = {};
                customFieldData.fields = {};

            $('.module-input').each(function () {

                let value = $(this).val();
                    name = $(this).attr('name');

                customFieldData.fields[name] = value;

            });

            $('#modules-content').val(JSON.stringify(customFieldData));

        });

        $('.img-field').on('change', function () {

            let value = $(this).val().replace('/../../frontend/web', '');
            $(this).val(value);
        });

    });

})(jQuery);