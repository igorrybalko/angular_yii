<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SimplePages */

$this->title = Yii::t('app', 'Create Simple Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Simple Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simple-pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
